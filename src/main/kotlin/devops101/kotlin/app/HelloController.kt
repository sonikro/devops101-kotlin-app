package devops101.kotlin.app

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces

@Controller("/")
class HelloController {
    @Get("/")
    @Produces(MediaType.TEXT_PLAIN)
    fun helloWorld(): String {
        Thread.sleep(1000) //Pretend we're doing something important
        return "Ola Mundo"
    }
}