package devops101.kotlin.app

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("devops101.kotlin.app")
                .mainClass(Application.javaClass)
                .start()
    }
}