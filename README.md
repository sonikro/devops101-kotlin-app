# devops101-kotlin-app

This app is the backend service for [devops101-react-app](https://gitlab.com/sonikro/devops101-react-app).

# Usage

```shell script
./gradlew clean run
```

# Building the docker Image

```shell script
./gradlew clen assemble
docker build -t devops101-kotlin-app .
```

# Running the Docker Image
```shell script
docker run -it -p 8080:8080 devops101-kotlin-app
```
